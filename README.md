# Description

This application consists of three screens: Registration, Authorization, and the Main Screen with two bottom bar tabs. The app uses local storage to save user data and BLoC for state management.

## Functionality

Registration Screen:

## Input login

Save login on the device using shared_preferences.
Authorization Screen:

## Input login.
Compare the entered login with the saved login.
If the login matches, proceed to the main screen.
Main Screen:

### Tab 1: Display data from any free API in a readable format.
### Tab 2: Display the login.
Additional Requirements
Initial check to determine if the user is logged in.
If logged in, proceed to the main screen.
If not logged in, show the registration/authorization screen.
Use BLoC for state management.
Installation and Setup
Clone the repository:

bash
Copy code

`git clone https://gitlab.com/ArmenSargsyan/dummy_shop`

`cd dummy_shop`

Install dependencies:

bash
Copy code
`flutter pub get`
Run the application:

bash \
Copy code\
`flutter run`

Dependencies
 > **shared_preferences**:
For local storage.

>**flutter_bloc**: For state management.

# Usage
**Registration Screen**: Enter a login and save it.\
**Authorization Screen**: Enter the saved login to proceed.\
**Main Screen**: View data from a free API on the first tab and your login on the second tab.