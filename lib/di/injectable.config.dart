// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:dio/dio.dart' as _i3;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../data/datasources/auth_local_data_source.dart' as _i4;
import '../data/repositories/api_repository_impl.dart' as _i8;
import '../data/repositories/auth_repository_impl.dart' as _i6;
import '../domain/repositories/i_api_repository.dart' as _i7;
import '../domain/repositories/i_auth_repository.dart' as _i5;
import '../presentation/blocs/auth/auth_bloc.dart' as _i9;
import '../presentation/blocs/products/products_fetcher_bloc.dart' as _i10;
import 'app_injectable_module.dart' as _i11;

// initializes the registration of main-scope dependencies inside of GetIt
_i1.GetIt $initGetIt(
  _i1.GetIt getIt, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    getIt,
    environment,
    environmentFilter,
  );
  final appInjectableModule = _$AppInjectableModule();
  gh.lazySingleton<_i3.Dio>(() => appInjectableModule.dio);
  gh.singleton<_i4.AuthLocalDataSource>(() => _i4.AuthLocalDataSourceImpl());
  gh.singleton<_i5.IAuthRepository>(
      () => _i6.AuthRepositoryImpl(gh<_i4.AuthLocalDataSource>()));
  gh.singleton<_i7.IApiRepository>(() => _i8.ApiRepositoryImpl(gh<_i3.Dio>()));
  gh.singleton<_i9.AuthBloc>(() => _i9.AuthBloc(gh<_i5.IAuthRepository>()));
  gh.singleton<_i10.ProductsFetcherBloc>(
      () => _i10.ProductsFetcherBloc(gh<_i7.IApiRepository>()));
  return getIt;
}

class _$AppInjectableModule extends _i11.AppInjectableModule {}
