import 'package:get_it/get_it.dart';
import 'package:test_app/presentation/blocs/auth/auth_bloc.dart';
import 'package:test_app/presentation/blocs/products/products_fetcher_bloc.dart';

abstract final class Locator {
  static final GetIt _getIt = GetIt.I;

  static AuthBloc get authBloc => _getIt<AuthBloc>();
  static ProductsFetcherBloc get productsFetcherBloc =>
      _getIt<ProductsFetcherBloc>();
}
