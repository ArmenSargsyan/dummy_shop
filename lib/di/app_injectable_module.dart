import 'package:injectable/injectable.dart';
import 'package:dio/dio.dart';

@module
abstract class AppInjectableModule {
  @LazySingleton()
  Dio get dio => Dio()..options.validateStatus = (status) => true;
}
