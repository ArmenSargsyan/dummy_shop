import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:test_app/di/injectable.config.dart';

@InjectableInit(
  initializerName: r'$initGetIt',
  preferRelativeImports: true,
  asExtension: false,
)
Future<GetIt> configureDependencies() async => $initGetIt(GetIt.instance);
