import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:test_app/domain/entities/user.dart';

part 'user_dto.g.dart';

@JsonSerializable()
class UserDto {
  final String login;

  const UserDto({
    required this.login,
  });

  factory UserDto.fromEntity(User user) {
    return UserDto(
      login: user.login,
    );
  }

  User toEntity() => User(
        login: login,
      );

  factory UserDto.formJson(Map<String, dynamic> json) =>
      _$UserDtoFromJson(json);

  Map<String, dynamic> toJson() => _$UserDtoToJson(this);
}
