import 'package:json_annotation/json_annotation.dart';
import 'package:test_app/domain/entities/rating_entity.dart';

part 'rating_dto.g.dart';

@JsonSerializable()
class RatingDto {
  final double rate;
  final int count;

  RatingDto({
    required this.rate,
    required this.count,
  });

  factory RatingDto.fromJson(Map<String, dynamic> json) =>
      _$RatingDtoFromJson(json);
  Map<String, dynamic> toJson() => _$RatingDtoToJson(this);

  RatingEntity toEntity() {
    return RatingEntity(
      rate: rate,
      count: count,
    );
  }
}
