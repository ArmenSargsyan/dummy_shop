import 'dart:developer';

import 'package:injectable/injectable.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:test_app/data/models/user_dto.dart';

abstract class AuthLocalDataSource {
  Future<void> saveUser(UserDto userDTO);
  Future<UserDto?> getUser();
  Future<void> clearUser();
}

@Singleton(as: AuthLocalDataSource)
class AuthLocalDataSourceImpl implements AuthLocalDataSource {
  @override
  Future<void> saveUser(UserDto userDTO) async {
    final prefs = await SharedPreferences.getInstance();
    try {
      await prefs.setString('login', userDTO.login);
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Future<UserDto?> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    final login = prefs.getString('login');

    if (login != null) {
      return UserDto(login: login);
    }
    return null;
  }

  @override
  Future<void> clearUser() {
    return SharedPreferences.getInstance()
        .then((prefs) => prefs.remove('login'));
  }
}
