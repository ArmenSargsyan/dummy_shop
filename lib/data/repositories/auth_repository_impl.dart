import 'package:injectable/injectable.dart';
import 'package:test_app/data/datasources/auth_local_data_source.dart';

import '../../domain/entities/user.dart';
import '../../domain/repositories/i_auth_repository.dart';
import '../models/user_dto.dart';

@Singleton(as: IAuthRepository)
class AuthRepositoryImpl implements IAuthRepository {
  final AuthLocalDataSource localDataSource;

  AuthRepositoryImpl(this.localDataSource);

  @override
  Future<void> saveLogin(String login) async {
    final userDTO = UserDto(login: login);
    await localDataSource.saveUser(userDTO);
  }

  @override
  Future<User?> getSavedLogin() async {
    final userDTO = await localDataSource.getUser();
    return userDTO?.toEntity();
  }

  @override
  Future<void> clearLogin() {
    return localDataSource.clearUser();
  }
}
