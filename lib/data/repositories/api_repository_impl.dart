import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:test_app/data/models/product_dto.dart';
import 'package:test_app/domain/entities/product.dart';
import 'package:test_app/domain/repositories/i_api_repository.dart';

@Singleton(as: IApiRepository)
class ApiRepositoryImpl implements IApiRepository {
  final Dio _dio;
  ApiRepositoryImpl(this._dio);

  @override
  Future<List<ProductEntity>> getProducts() async {
    final response = await _dio.get('https://fakestoreapi.com/products');

    List productsData = response.data;

    final products = productsData
        .map((product) => ProductDto.fromJson(product).toEntity())
        .toList();

    return products;
  }
}
