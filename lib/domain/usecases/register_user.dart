import 'package:test_app/domain/repositories/i_auth_repository.dart';

class RegisterUser {
  final IAuthRepository repository;

  RegisterUser(this.repository);

  Future<void> call(String login) async {
    await repository.saveLogin(login);
  }
}
