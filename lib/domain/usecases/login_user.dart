import '../entities/user.dart';
import '../repositories/i_auth_repository.dart';

class LoginUser {
  final IAuthRepository repository;

  LoginUser(this.repository);

  Future<User?> call(String login) async {
    final user = await repository.getSavedLogin();
    if (user != null && user.login == login) {
      return user;
    }
    return null;
  }
}
