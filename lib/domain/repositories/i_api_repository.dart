import 'package:test_app/domain/entities/product.dart';

abstract interface class IApiRepository {
  Future<List<ProductEntity>> getProducts();
}
