import 'package:test_app/domain/entities/user.dart';

abstract interface class IAuthRepository {
  Future<void> saveLogin(String login);
  Future<User?> getSavedLogin();
  Future<void> clearLogin();
}
