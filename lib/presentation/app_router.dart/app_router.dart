import 'package:auto_route/auto_route.dart';
import 'package:test_app/presentation/pages/auth_screen.dart';
import 'package:test_app/presentation/pages/base%20_screen.dart';
import 'package:test_app/presentation/pages/home_screen.dart';
import 'package:test_app/presentation/pages/init_screen.dart';
import 'package:test_app/presentation/pages/profile_screen.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: InitRoute.page, initial: true),
        AutoRoute(page: AuthRoute.page),
        AutoRoute(
          path: '/base',
          page: BaseRoute.page,
          children: [
            AutoRoute(path: 'home', page: HomeRoute.page),
            AutoRoute(path: 'profile', page: ProfileRoute.page),
          ],
        ),
      ];
}
