part of 'products_fetcher_bloc.dart';

@freezed
class ProductsFetcherState with _$ProductsFetcherState {
  const factory ProductsFetcherState.initial() = Initial;
  const factory ProductsFetcherState.loading() = Loading;
  const factory ProductsFetcherState.loaded({
    required List<ProductEntity> products,
  }) = Loaded;
  const factory ProductsFetcherState.error(String message) = Error;
}
