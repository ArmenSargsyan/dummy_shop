part of 'products_fetcher_bloc.dart';

@freezed
class ProductsFetcherEvent with _$ProductsFetcherEvent {
  const factory ProductsFetcherEvent.fetch() = Fetch;
}
