import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:test_app/domain/entities/product.dart';
import 'package:test_app/domain/repositories/i_api_repository.dart';

part 'products_fetcher_event.dart';
part 'products_fetcher_state.dart';
part 'products_fetcher_bloc.freezed.dart';

@singleton
class ProductsFetcherBloc
    extends Bloc<ProductsFetcherEvent, ProductsFetcherState> {
  final IApiRepository _productsRepository;

  ProductsFetcherBloc(this._productsRepository)
      : super(const ProductsFetcherState.initial()) {
    on<ProductsFetcherEvent>(_onProductsFetcherEvent);
  }
  Future<void> _onProductsFetcherEvent(
    ProductsFetcherEvent event,
    Emitter<ProductsFetcherState> emit,
  ) async {
    emit(const ProductsFetcherState.loading());
    try {
      final products = await _productsRepository.getProducts();
      emit(ProductsFetcherState.loaded(products: products));
    } catch (e) {
      emit(ProductsFetcherState.error(e.toString()));
    }
  }
}
