part of 'auth_bloc.dart';

@freezed
class AuthEvent with _$AuthEvent {
  factory AuthEvent.register({required String login}) = _Register;
  factory AuthEvent.login({required String login}) = _Login;
  factory AuthEvent.logout() = _Logout;
  factory AuthEvent.checkAuth() = _CheckAuth;
}
