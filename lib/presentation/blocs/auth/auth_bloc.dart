import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:test_app/domain/repositories/i_auth_repository.dart';

part 'auth_bloc.freezed.dart';
part 'auth_event.dart';
part 'auth_state.dart';

@singleton
class AuthBloc extends Bloc<AuthEvent, AuthState> {
  IAuthRepository repository;
  AuthBloc(this.repository) : super(const AuthState.initial()) {
    on<_CheckAuth>(_onCheckAuth);
    on<_Register>(_onRegister);
    on<_Login>(_onLogin);
    on<_Logout>(_onLogout);
  }

  Future<void> _onRegister(_Register event, Emitter<AuthState> emit) async {
    emit(const AuthState.loading());
    try {
      await repository.saveLogin(event.login);
      emit(AuthState.authenticated(login: event.login));
    } catch (e) {
      emit(AuthState.error(e.toString()));
    }
  }

  Future<void> _onCheckAuth(_CheckAuth event, Emitter<AuthState> emit) async {
    emit(const AuthState.loading());
    try {
      final user = await repository.getSavedLogin();
      if (user != null) {
        emit(AuthState.authenticated(login: user.login));
      } else {
        emit(const AuthState.unauthenticated());
      }
    } catch (e) {
      emit(AuthState.error(e.toString()));
    }
  }

  Future<void> _onLogin(_Login event, Emitter<AuthState> emit) async {
    emit(const AuthState.loading());
    try {
      final user = await repository.getSavedLogin();
      if (user != null && user.login == event.login) {
        emit(AuthState.authenticated(login: event.login));
        return;
      }
      emit(const AuthState.unauthenticated());
    } catch (e) {
      emit(AuthState.error(e.toString()));
    }
  }

  Future<void> _onLogout(_Logout event, Emitter<AuthState> emit) async {
    emit(const AuthState.loading());
    try {
      // await repository.clearLogin();
      emit(const AuthState.unauthenticated());
    } catch (e) {
      emit(AuthState.error(e.toString()));
    }
  }
}
