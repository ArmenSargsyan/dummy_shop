import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/presentation/blocs/products/products_fetcher_bloc.dart';
import 'package:test_app/presentation/pages/product_tile.dart';

@RoutePage()
class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[100],
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Home'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Center(
          child: BlocBuilder<ProductsFetcherBloc, ProductsFetcherState>(
              builder: (context, productsFetcherState) {
            return productsFetcherState.maybeWhen(
              loading: () {
                return const CircularProgressIndicator();
              },
              loaded: (products) {
                return ListView.builder(
                  itemCount: products.length,
                  itemBuilder: (context, index) {
                    return ProductTile(product: products[index]);
                  },
                );
              },
              orElse: () {
                return const Center(
                  child: Text('No products'),
                );
              },
            );
          }),
        ),
      ),
    );
  }
}
