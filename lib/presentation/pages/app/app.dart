
import 'package:flutter/material.dart';
import 'package:test_app/presentation/app_router.dart/app_router.dart';
import 'package:test_app/presentation/pages/app/app_global_prviders.dart';

class App extends StatelessWidget {
  const App({super.key});
  static final AppRouter appRouter = AppRouter();
  @override
  Widget build(BuildContext context) {
    return AppGlobalPrviders(
      child: MaterialApp.router(
        title: 'Flutter Test App',
        routerConfig: appRouter.config(),
      ),
    );
  }
}
