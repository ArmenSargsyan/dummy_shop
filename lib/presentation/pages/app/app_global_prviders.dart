import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/di/locator.dart';
import 'package:test_app/presentation/blocs/auth/auth_bloc.dart';

class AppGlobalPrviders extends StatelessWidget {
  final Widget child;

  const AppGlobalPrviders({super.key, required this.child});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (context) => Locator.authBloc
            ..add(
              AuthEvent.checkAuth(),
            ),
        ),
      ],
      child: child,
    );
  }
}
