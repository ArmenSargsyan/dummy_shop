import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/presentation/app_router.dart/app_router.dart';
import 'package:test_app/presentation/blocs/auth/auth_bloc.dart';

@RoutePage()
class InitScreen extends StatefulWidget {
  const InitScreen({super.key});

  @override
  State<InitScreen> createState() => _InitScreenState();
}

class _InitScreenState extends State<InitScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, authState) {
          authState.maybeWhen(
            loading: () {},
            authenticated: (_) {
              context.router.replace(const BaseRoute());
            },
            orElse: () {
              context.router.replace(const AuthRoute());
            },
          );
          return const Center(
            child: Column(
              children: [
                Text('Init Screen'),
                SizedBox.square(
                  dimension: 40,
                  child: CircularProgressIndicator(),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
