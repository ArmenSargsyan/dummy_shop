import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_app/presentation/app_router.dart/app_router.dart';
import 'package:test_app/presentation/blocs/auth/auth_bloc.dart';

@RoutePage()
class AuthScreen extends StatefulWidget {
  const AuthScreen({super.key});

  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final TextEditingController _loginController = TextEditingController();
  final FocusNode _loginFocus = FocusNode();
  bool isSignUp = false;
  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        state.mapOrNull(
          authenticated: (_) {
            context.router.replace(const BaseRoute());
          },
        );
      },
      child: Scaffold(
        body: Center(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                const Spacer(),
                Text(
                  isSignUp ? "Sign up" : "Welcome back!", // TODO: intl
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                // const SizedBox(height: 20),
                const Spacer(),
                TextField(
                  controller: _loginController,
                  focusNode: _loginFocus,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Login',
                  ),
                  cursorColor: Colors.blue[900],
                ),
                const SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () {
                    if (isSignUp) {
                      context.read<AuthBloc>().add(AuthEvent.register(
                          login: _loginController.text.trim()));
                    } else {
                      context.read<AuthBloc>().add(
                          AuthEvent.login(login: _loginController.text.trim()));
                    }
                  },
                  child: Text(isSignUp ? 'Sign Up' : 'Sign In'),
                ),
                const Spacer(
                  flex: 2,
                ),
                //switcher to login screen
                SizedBox(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        isSignUp
                            ? 'Already a member?'
                            : "Don't have registration yet?",
                      ),
                      const SizedBox(width: 5),
                      TextButton(
                        onPressed: () {
                          setState(
                            () {
                              isSignUp = !isSignUp;
                            },
                          );
                        },
                        child: Text(
                          isSignUp ? 'Sign In' : 'Sign Up', // TODO: intl
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).viewPadding.bottom + 15)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
