import 'package:flutter/material.dart';
import 'package:test_app/di/injectable.dart';
import 'package:test_app/presentation/pages/app/app.dart';

void main() async {
  await configureDependencies();
  runApp(const App());
}
